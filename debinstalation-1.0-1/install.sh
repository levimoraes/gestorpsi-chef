#!/bin/bash

sudo apt-get install git
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
echo 'source `which virtualenvwrapper.sh`'
source $HOME/.bashrc
sudo pip install virtualenvwrapper
mkvirtualenv gestorpsi
pip install ipython flake8
git clone https://github.com/GestorPsi-MES-12015/gestorpsi.git
wget c758482.r82.cf2.rackcdn.com/sublime-text_build-3083_i386.deb
sudo dpkg -i sublime-text_build-3083_i386.deb
